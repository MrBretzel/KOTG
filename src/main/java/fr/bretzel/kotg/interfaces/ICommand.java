package fr.bretzel.kotg.interfaces;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Loïc Nussbaumer on 10/03/2017.
 */
public abstract class ICommand implements CommandExecutor, TabCompleter {

    public boolean onCommandByPlayer(Player sender, Command command, String[] args) {
        return false;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player)
            return onCommandByPlayer((Player) sender, command, args);
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return null;
    }
}

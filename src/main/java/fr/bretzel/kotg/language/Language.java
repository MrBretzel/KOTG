package fr.bretzel.kotg.language;

import com.google.common.base.Strings;
import fr.bretzel.kotg.KOTG;
import fr.bretzel.kotg.util.ComponentUtil;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.io.*;
import java.util.HashMap;
import java.util.zip.ZipException;

/**
 * Created by MrBretzel on 25/03/2017.
 */
public class Language {

    private HashMap<String, BaseComponent[]> stringToComponent = new HashMap<>();
    private Locale locale = Locale.EN;

    public static Language defaultLanguage;

    private static HashMap<Locale, Language> localeToLanguage = new HashMap<>();

    public Language(Locale locale) {
        this.locale = locale;
    }

    public boolean has(String k) {
        if (stringToComponent.containsKey(k))
            return true;
        if (defaultLanguage == null)
            return false;
        return defaultLanguage.stringToComponent.containsKey(k);
    }

    public BaseComponent[] get(String k) {
        if (has(k))
            return stringToComponent.get(k);
        else
            return defaultLanguage.get(k);
    }

    /**
     * This method return to language is local is null return to the default language !
     * @param locale
     * @return a language
     */
    public static Language getLanguage(Locale locale) {
        if (localeToLanguage.containsKey(locale))
            return localeToLanguage.get(locale);
        return defaultLanguage;
    }

    public String getLanguageName() {
        return locale.name();
    }

    public static void enable() throws IOException {
        for (Locale locale : Locale.values()) {
            Language language = new Language(locale);
            String path = "/lang/" + locale.name().toLowerCase() + "_" + locale.name() + ".lang".trim();
            InputStream input = KOTG.class.getResourceAsStream(path);
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            String line;
            try {
                while ((line = reader.readLine()) != null) {

                    if (Strings.isNullOrEmpty(line) || line.charAt(0) == '#') {
                        continue;
                    }

                    String[] args = line.split("=");
                    String k = args[0];
                    KeyType type = KeyType.fromString(args[1]);
                    String value = args[2];

                    if (type == KeyType.JSON) {
                        if (language.has(k)) {
                            Bukkit.getLogger().info("The key: " + k + " is already init !");
                        } else {
                            language.stringToComponent.put(k, ComponentUtil.toBaseComponent(value));
                        }
                    } else if (type == KeyType.TXT) {
                        if (language.has(k)) {
                            Bukkit.getLogger().info("The key: " + k + " is already init !");
                        } else {
                            language.stringToComponent.put(k, new BaseComponent[]{new TextComponent(ChatColor.translateAlternateColorCodes('&', value))});
                        }
                    }
                }
                reader.close();
                localeToLanguage.put(locale, language);
            } catch (Exception e) {}
        }

        Language.defaultLanguage = Language.getLanguage(Locale.EN);

        Bukkit.broadcastMessage(defaultLanguage.getLanguageName());
    }

    public enum KeyType {
        JSON,
        TXT;

        public static KeyType fromString(String s) {
            switch (s.trim()) {
                case "TXT":
                    return KeyType.TXT;
                case "JSON":
                    return KeyType.JSON;
                default:
                    return KeyType.TXT;
            }
        }
    }

    public enum Locale {
        FR,
        EN;

        public static Locale fromString(String s) {
            switch (s.trim()) {
                case "EN" :case "en":case "en_EN":
                    return Locale.EN;
                case "FR":case "fr":case "fr_FR":
                    return Locale.FR;
                default:
                    return Locale.EN;
            }
        }
    }
}

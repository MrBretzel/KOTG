package fr.bretzel.kotg.player;

import com.google.common.base.Strings;
import fr.bretzel.kotg.KOTG;
import fr.bretzel.kotg.interfaces.IClass;
import fr.bretzel.kotg.language.Language;
import fr.bretzel.kotg.util.FilesUtils;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Loïc Nussbaumer on 11/03/2017.
 */
public class PlayerInfo {

    private IClass classe;
    private ArrayList<UUID> friends = new ArrayList<>();
    private UUID player;
    private Language language = Language.defaultLanguage;

    public static HashMap<UUID, PlayerInfo> playerInfos = new HashMap<>();

    private PlayerInfo(Player player) {
        this.player = player.getUniqueId();
        load();
        PlayerInfo.playerInfos.put(player.getUniqueId(), this);
    }

    public List<UUID> getFriends() {
        return friends;
    }

    public void addFriend(Player player) {
        addFriend(player.getUniqueId());
    }

    public void addFriend(UUID uuid) {
        if (!hasFriend(uuid) && !uuid.toString().equalsIgnoreCase(getPlayer().toString()))
            getFriends().add(uuid);
    }

    public void removeFriend(Player player) {
        removeFriend(player.getUniqueId());
    }

    public void removeFriend(UUID uuid) {
        if (hasFriend(uuid))
            getFriends().remove(uuid);
    }

    public boolean hasFriend(UUID uuid) {
        return getFriends().contains(uuid);
    }

    public boolean hasFriend(Player player) {
        return hasFriend(player.getUniqueId());
    }

    public UUID getPlayer() {
        return player;
    }

    public IClass getClasse() {
        return classe;
    }

    public void setClasse(IClass classe) {
        this.classe = classe;
    }

    public static PlayerInfo get(Player player) {
        if (playerInfos.containsKey(player.getUniqueId()))
            return playerInfos.get(player.getUniqueId());
        return new PlayerInfo(player);
    }

    public static void removePlayerInfo(Player player) {
        if (playerInfos.containsKey(player.getUniqueId())) {
            playerInfos.remove(player.getUniqueId());
        }
    }

    public Language getLanguage() {
        if (this.language == null) {
            setLanguage(Language.defaultLanguage);
        }
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public static void saveAll() {
        File directory = new File(KOTG.BASE_DIRECTORY, File.separator + "players");

        if (!directory.exists())
            FilesUtils.createNewDirectory(directory);

        for (PlayerInfo info : PlayerInfo.playerInfos.values()) {
            info.save();
        }
    }

    public void save() {
        File directory = new File(KOTG.BASE_DIRECTORY, File.separator + "players");

        File file = new File(directory, getPlayer().toString() + ".yml");

        if (!file.exists())
            FilesUtils.createNewFile(file);

        YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);
        configuration.set("Language", getLanguage().getLanguageName());
        configuration.set("Friends", null);
        ConfigurationSection section = configuration.createSection("Friends");

        if (getFriends().size() > 0) {
            for (int i = 0; getFriends().get(i) != null; i++) {
                section.set("Friend: " + String.valueOf(i), getFriends().get(i).toString());
            }
        }

        configuration.set("Friends", section);

        try {
            configuration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        File directory = new File(KOTG.BASE_DIRECTORY, File.separator + "players");

        if (!directory.exists())
            FilesUtils.createNewDirectory(directory);

        File file = new File(directory, getPlayer().toString() + ".yml");

        if (!file.exists())
            FilesUtils.createNewFile(file);

        YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);

        Language.Locale locale = Language.Locale.fromString(configuration.getString("Language"));

        if (locale == null)
            setLanguage(Language.defaultLanguage);
        else
            setLanguage(Language.getLanguage(locale));

        ConfigurationSection friends = configuration.getConfigurationSection("Friends");

        if (friends != null) {
            for (int i = 0; friends.isSet("Friend: " + String.valueOf(i)); i++) {
                String s = friends.getString(String.valueOf(i));
                Bukkit.broadcastMessage("Added new friend for player: " + Bukkit.getPlayer(getPlayer()).getName());
                addFriend(UUID.fromString(s));
            }
        }
    }
}

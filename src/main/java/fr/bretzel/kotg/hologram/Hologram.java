/**
 * Copyright 2017 Loïc Nussbaumer
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License. You
 * may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License. See accompanying
 * LICENSE file.
 */
package fr.bretzel.kotg.hologram;

import org.bukkit.Location;
import org.bukkit.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mrbretzel on 13/07/15.
 */

public class Hologram {

    private HoloEntity[] holoEntities = {};
    private String[] lines = {};
    private Location[] locations = {};
    private Location location;
    private HologramManager holomanager;
    private boolean visible = false;

    public Hologram(World world, double x, double y, double z, String[] lines, HologramManager manager) {
        this(new Location(world, x, y, z), lines, 0.5, manager);
    }

    public Hologram(Location location, String line, HologramManager manager) {
        this(location, new String[]{line}, 0.5, manager);
    }

    public Hologram(Location location, String line, double spacer, HologramManager manager) {
        this(location, new String[]{line}, 0.5, manager);
    }

    public Hologram(World world, double x, double y, double z, String line, HologramManager manager) {
        this(new Location(world, x, y, z), line, manager);
    }

    public Hologram(World world, double x, double y, double z, String line, double spacer, HologramManager manager) {
        this(new Location(world, x, y, z), line, manager);
    }

    public Hologram(Location location, String[] lines, double spacer, HologramManager manager) {
        if(manager == null) {
            throw new NullPointerException("The hologram manager is not instanced !");
        }

        this.holomanager = manager;
        setLines(lines);
        setLocation(location);
        double yAdd = 0.0;
        List<Location> locs = new ArrayList<>();
        List<HoloEntity> holos = new ArrayList<>();
        for (int i = getLines().length -1; i >= 0; i--) {
            String s = getLines()[i];
            Location loc = location.clone().add(0.0, yAdd, 0.0);
            locs.add(loc);
            yAdd += spacer;
            HoloEntity entity = new HoloEntity(loc, s, getHolomanager().getPlugin());
            holos.add(entity);
        }
        setHoloEntities(holos.toArray(new HoloEntity[holos.size()]));
        setLocations(locs.toArray(new Location[locs.size()]));
        manager.getHologramList().add(this);
    }

    public void display(boolean b) {
        this.visible = b;
        for(HoloEntity e : getHoloEntities()) {
            e.getStand().setCustomNameVisible(b);
        }
    }

    public boolean isVisible() {
        return this.visible;
    }

    public HoloEntity[] getHoloEntities() {
        return holoEntities;
    }

    public void setHoloEntities(HoloEntity[] holoEntities) {
        this.holoEntities = holoEntities;
    }

    public Location[] getLocations() {
        return locations;
    }

    public void setLocations(Location[] locations) {
        this.locations = locations;
    }

    public String[] getLines() {
        return lines;
    }

    public void setLines(String[] lines) {
        this.lines = lines;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public HologramManager getHolomanager() {
        return holomanager;
    }

    public void remove() {
        for(HoloEntity entity : getHoloEntities()) {
            entity.getStand().remove();
        }
    }
}

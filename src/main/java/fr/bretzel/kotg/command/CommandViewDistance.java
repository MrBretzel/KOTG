package fr.bretzel.kotg.command;

import fr.bretzel.kotg.interfaces.ICommand;
import fr.bretzel.kotg.language.Language;
import fr.bretzel.kotg.player.PlayerInfo;
import fr.bretzel.kotg.util.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Loïc Nussbaumer on 14/03/2017.
 */
public class CommandViewDistance extends ICommand {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player && args.length > 0) {
            Player player = (Player) sender;
            Language lang = PlayerInfo.get(player).getLanguage();
            if (Utils.isInteger(args[0])) {
                int viewDistance = Integer.parseInt(args[0]);

                if (viewDistance > 16) {
                    sender.sendMessage(lang.get("command.viewdistance.error.1"));
                    return false;
                }
                player.setViewDistance(viewDistance);
                sender.sendMessage(lang.get("command.viewdistance.success.1")[0].toPlainText().replace("%distance%", String.valueOf(viewDistance)));
                return true;
            } else if (Bukkit.getPlayer(args[0]) != null){
                Player p = Bukkit.getPlayer(args[0]);
                if (args.length > 1) {
                    if (Utils.isInteger(args[1])) {
                        int viewDistance = Integer.parseInt(args[1]);

                        if (viewDistance > 16) {
                            sender.sendMessage(lang.get("command.viewdistance.error.1"));
                            return false;
                        }
                        p.setViewDistance(viewDistance);
                        sender.sendMessage(lang.get("command.viewdistance.success.2")[0].toPlainText().replace("%distance%", String.valueOf(viewDistance)).replace("%player%", p.getName()));
                        return true;
                    } else {
                        sender.sendMessage(lang.get("command.viewdistance.player.usage"));
                        return false;
                    }
                } else {
                    sender.sendMessage(lang.get("command.viewdistance.player.usage"));
                    return false;
                }
            } else {
                sender.sendMessage(lang.get("command.viewdistance.usage"));
                return false;
            }
        } else {
            sender.sendMessage(Language.defaultLanguage.get("command.viewdistance.usage"));
            return false;
        }
    }
}

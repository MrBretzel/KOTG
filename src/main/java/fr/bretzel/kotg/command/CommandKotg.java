package fr.bretzel.kotg.command;

import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.factions.entity.MPlayer;
import fr.bretzel.kotg.KOTG;
import fr.bretzel.kotg.zone.Selection;
import fr.bretzel.kotg.interfaces.ICommand;
import fr.bretzel.kotg.zone.Zone;
import fr.bretzel.kotg.zone.ZoneManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Loïc Nussbaumer on 10/03/2017.
 */
public class CommandKotg extends ICommand {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length > 0) {
                if (args[0].equalsIgnoreCase("zone")) {
                    if (args.length > 1) {
                        if (args[1].equalsIgnoreCase("create")) {
                            if (args.length > 2) {
                                String name = args[2];
                                if (ZoneManager.hasZone(name)) {
                                    player.sendMessage(ChatColor.RED + "The name : " + name + " is already created ! !");
                                    return true;
                                } else {
                                    Selection selection = KOTG.getSelection(player.getUniqueId());
                                    if (selection.getLoc1() == null && selection.getLoc2() == null) {
                                        player.sendMessage(ChatColor.RED + "Please selection your two point !");
                                        return false;
                                    } else if (selection.getLoc1() == null) {
                                        player.sendMessage(ChatColor.RED + "Please selection your first point !");
                                        return false;
                                    } else if (selection.getLoc2() == null) {
                                        player.sendMessage(ChatColor.RED + "Please selection your second point !");
                                        return false;
                                    } else {
                                        ZoneManager.addZone(name, selection);
                                        player.sendMessage(ChatColor.GREEN + "You are created a new zone by name key: " + ChatColor.DARK_GREEN + name);
                                        return true;
                                    }
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Usage: /kotg zone create <name>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("list")) {
                            if (ZoneManager.listZize() > 0) {
                                player.sendMessage(ChatColor.GREEN + "Zone list: ");
                                for (Zone zone : ZoneManager.getZones()) {
                                    player.sendMessage(ChatColor.GREEN + "   Name: " + ChatColor.AQUA + zone.getName());
                                    player.sendMessage(ChatColor.GREEN + "      Chunk claimed: " + ChatColor.AQUA + zone.getChunks().size());
                                }
                                return true;
                            } else {
                                player.sendMessage(ChatColor.RED + "Please create a Zone !");
                                return true;
                            }
                        } else if (args[1].equalsIgnoreCase("delete")) {
                            if (args.length > 2) {
                                String name = args[2];
                                if (!ZoneManager.hasZone(name)) {
                                    player.sendMessage(ChatColor.RED + "Cant not be found " + name + " !");
                                    return false;
                                } else {
                                    ZoneManager.removeZone(name);
                                    player.sendMessage(ChatColor.GREEN + "Successfully deleted " + name + " !");
                                    return true;
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Usage: /kotg zone delete <name>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("display")) {
                            if (args.length > 2) {
                                String name = args[2];
                                if (ZoneManager.hasZone(name)) {
                                    Zone zone = ZoneManager.getZone(name);
                                    zone.setDisplay(!zone.isDisplay());
                                    player.sendMessage(ChatColor.GREEN + "The chunk has been displayed !");
                                    return true;
                                } else {
                                    player.sendMessage(ChatColor.RED + "Cant not be found the zone: " + name);
                                    return false;
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Usage: /kotg zone display <name>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("add")) {
                            if (args.length > 2) {
                                String name = args[2];
                                if (ZoneManager.hasZone(name)) {
                                    Zone zone = ZoneManager.getZone(name);
                                    if (!zone.hasChunk(player.getLocation())) {
                                        zone.addChunk(player.getLocation());
                                        player.sendMessage(ChatColor.GREEN + "The chunk has been added to " + name);
                                        return true;
                                    } else {
                                        player.sendMessage(ChatColor.RED + "The chunk is already claimed !");
                                        return false;
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "Cant be found the zone " + name);
                                    return false;
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Usage: /kotg zone add <name>");
                                return false;
                            }
                        } else if (args[1].equalsIgnoreCase("remove")) {
                            if (args.length > 2) {
                                String name = args[2];
                                if (ZoneManager.hasZone(name)) {
                                    Zone zone = ZoneManager.getZone(name);
                                    if (zone.hasChunk(player.getLocation())) {
                                        zone.removeChunk(player.getLocation());
                                        player.sendMessage(ChatColor.GREEN + "The chunk has been removed to " + name);
                                        return true;
                                    } else {
                                        player.sendMessage(ChatColor.RED + "The chunk is not in the zone !");
                                        return false;
                                    }
                                } else {
                                    player.sendMessage(ChatColor.RED + "Cant be found the zone " + name);
                                    return false;
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Usage: /kotg zone add <name>");
                                return false;
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Usage: /kotg zone <create|list|delete|display|add|remove>");
                            return false;
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Usage: /kotg zone <create|list|delete|display|add|remove>");
                        return false;
                    }
                } else if (args[0].equalsIgnoreCase("claim")) {
                    if (args.length > 1) {
                        MPlayer mPlayer = MPlayer.get(player.getUniqueId());
                        Faction faction = mPlayer.getFaction();
                        faction.getMPlayersWhereOnline(false).size();
                        String zoneName = args[1];
                        if (!ZoneManager.hasZone(zoneName)) {
                            sender.sendMessage(ChatColor.RED + "Cant not be found zone: " + zoneName + " !");
                            return false;
                        }
                        if (faction == null) {
                            sender.sendMessage(ChatColor.RED + "You are not in a faction !");
                            return false;
                        }
                        if (args.length > 2) {
                            if (FactionColl.get().getByName(args[2]) != null) {
                                faction = FactionColl.get().getByName(args[2]);
                                KOTG.addZoneToFaction(faction, ZoneManager.getZone(zoneName));
                                sender.sendMessage(ChatColor.GREEN + "All chunk in " + zoneName + " has been claim to: " + faction.getName());
                                return true;
                            } else {
                                sender.sendMessage(ChatColor.RED + "Cant not be found faction: " + args[1] + " !");
                                return false;
                            }
                        } else {
                            KOTG.addZoneToFaction(faction, ZoneManager.getZone(zoneName));
                            sender.sendMessage(ChatColor.GREEN + "All chunk in " + zoneName + " has been claim to your faction !");
                            return true;
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "usage: /kotg claim <zone> or /kotg claim <zone> <faction>");
                        return false;
                    }
                } else if (args[0].equalsIgnoreCase("unclaim")) {
                    if (args.length > 1) {
                        String zoneName = args[1];
                        if (ZoneManager.hasZone(zoneName)) {
                            Zone zone = ZoneManager.getZone(zoneName);
                            if (zone.isClaimed()) {
                                KOTG.removeZoneToFaction(ZoneManager.getZone(zoneName));
                                sender.sendMessage(ChatColor.GREEN + "All chunk in " + zoneName + " has been unclaim !");
                                return true;
                            } else {
                                sender.sendMessage(ChatColor.RED + "The zone " + zoneName + " is not claimed by a faction !");
                                return true;
                            }
                        } else {
                            sender.sendMessage(ChatColor.RED + "Cant not be found zone: " + zoneName + " !");
                            return false;
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "usage: /kotg unclaim <zone> or /kotg unclaim <zone> <faction>");
                        return false;
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Usage: /kotg <zone|claim|unclaim>");
                    return false;
                }
            } else {
                player.sendMessage(ChatColor.RED + "Usage: /kotg <zone|claim|unclaim>");
                return false;
            }
        }
        return false;
    }
}

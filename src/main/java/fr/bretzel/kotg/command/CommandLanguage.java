package fr.bretzel.kotg.command;

import fr.bretzel.kotg.interfaces.ICommand;
import fr.bretzel.kotg.language.Language;
import fr.bretzel.kotg.player.PlayerInfo;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

/**
 * Created by MrBretzel on 28/03/2017.
 */
public class CommandLanguage extends ICommand {

    @Override
    public boolean onCommandByPlayer(Player sender, Command command, String[] args) {
        PlayerInfo info = PlayerInfo.get(sender);
        if (args.length > 0) {
            if (Language.Locale.fromString(args[0]) != null) {
                Language.Locale locale = Language.Locale.fromString(args[0]);
                info.setLanguage(Language.getLanguage(locale));
                sender.sendMessage(info.getLanguage().get("command.language.success")[0].toPlainText().replace("%language%", locale.name()));
            } else {
                sender.sendMessage(info.getLanguage().get("command.language.error"));
                return false;
            }
        } else {
            sender.sendMessage(info.getLanguage().get("command.language.usage"));
            return false;
        }
        return false;
    }
}

package fr.bretzel.kotg.command;

import fr.bretzel.kotg.friend.FriendManager;
import fr.bretzel.kotg.friend.FriendRequest;
import fr.bretzel.kotg.interfaces.ICommand;
import fr.bretzel.kotg.inventory.InventoryManager;
import fr.bretzel.kotg.language.Language;
import fr.bretzel.kotg.player.PlayerInfo;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

/**
 * Created by Loïc Nussbaumer on 13/03/2017.
 */
public class CommandFriend extends ICommand {

    @Override
    public boolean onCommandByPlayer(Player player, Command command, String[] args) {
        PlayerInfo info = PlayerInfo.get(player);
        Language lang = info.getLanguage();
        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("add")) {
                if (args.length > 1) {
                    Player p = Bukkit.getPlayer(args[1]);
                    if (p == null) {
                        player.sendMessage(lang.get("util.player.not.found"));
                        return false;
                    } else if (!p.isOnline()) {
                        player.sendMessage(lang.get("util.player.not.online"));
                        return false;
                    } else if (info.hasFriend(p)) {
                        player.sendMessage(lang.get("command.friend.already"));
                        return false;
                    } else if (p.getUniqueId().toString().equalsIgnoreCase(player.getUniqueId().toString())) {
                        player.sendMessage(lang.get("command.friend.add.itsyou"));
                        return false;
                    } else {
                        FriendRequest request = new FriendRequest(player, p);
                        p.sendMessage(request.getSendMessage());

                        FriendManager.requestFriend(request);

                        String msg = lang.get("command.friend.add.success")[0].toPlainText();

                        player.sendMessage(msg.replace("%player%", p.getName()));
                        return true;
                    }
                } else {
                    player.sendMessage(lang.get("command.friend.add.usage"));
                    return false;
                }
            } else if (args[0].equalsIgnoreCase("remove")) {
                if (args.length > 1) {
                    Player p = Bukkit.getPlayer(args[1]);
                    if (p == null) {
                        player.sendMessage(lang.get("util.player.not.found"));
                        return false;
                    } else if (!info.hasFriend(p)) {
                        player.sendMessage(lang.get("command.friend.already"));
                        return false;
                    } else {
                        FriendManager.remove(player, p);

                        String msg1 = PlayerInfo.get(p).getLanguage().get("command.friend.remove.success.1")[0].toPlainText().replace("%player", player.getName());
                        String msg2 = PlayerInfo.get(player).getLanguage().get("command.friend.remove.success.2")[0].toPlainText().replace("%player%", p.getName());

                        p.sendMessage(msg1);
                        player.sendMessage(msg2);
                        return true;
                    }
                } else {
                    player.sendMessage(lang.get("command.friend.remove.usage"));
                    return false;
                }
            } else if (args[0].equalsIgnoreCase("list")) {
                //TODO
                player.sendMessage("TODO i recoded this !");
                //InventoryManager.openFriendList(info);
                return true;
            } else if (args[0].equalsIgnoreCase("accept")) {
                if (FriendManager.hasReceiver(player)) {
                    if (args.length > 1) {
                        Player p = Bukkit.getPlayer(args[1]);
                        FriendRequest r = FriendManager.getRequestReceiver(player);
                        if (p == null) {
                            player.sendMessage(lang.get("util.player.not.found"));
                        } else if (!p.isOnline()) {
                            player.sendMessage(lang.get("util.player.not.online"));
                            return false;
                        } else if (r.isExpired()) {
                            FriendManager.requestFriend(r);
                            player.sendMessage(lang.get("command.friend.request.expired"));
                            return false;
                        } else {
                            FriendManager.accept(player);
                            String txt = PlayerInfo.get(p).getLanguage().get("command.friend.accept.success")[0].toPlainText().replace("%player", player.getName());

                            p.sendMessage(txt);

                            txt = PlayerInfo.get(player).getLanguage().get("command.friend.accept.success")[0].toPlainText().replace("%player", p.getName());

                            player.sendMessage(txt);
                            return true;
                        }
                    } else {
                        player.sendMessage(lang.get("command.friend.accept.usage"));
                        return false;
                    }
                } else {
                    player.sendMessage(lang.get("command.friend.norequest"));
                    return false;
                }
            } else if (args[0].equalsIgnoreCase("deny")) {
                if (FriendManager.hasReceiver(player)) {
                    if (args.length > 1) {
                        Player p = Bukkit.getPlayer(args[1]);
                        FriendRequest r = FriendManager.getRequestReceiver(player);
                        if (p == null) {
                            player.sendMessage(lang.get("util.player.not.found"));
                        } else if (!p.isOnline()) {
                            player.sendMessage(lang.get("util.player.not.online"));
                            return false;
                        } else if (r.isExpired()) {
                            FriendManager.requestFriend(r);
                            player.sendMessage(lang.get("command.friend.request.expired"));
                            return false;
                        } else {
                            FriendManager.denny(player);
                            p.sendMessage(PlayerInfo.get(p).getLanguage().get("command.friend.deny.success.1")[0].toPlainText().replace("%player", player.getName()));
                            player.sendMessage(PlayerInfo.get(player).getLanguage().get("command.friend.deny.success.2"));
                            return true;
                        }
                    } else {
                        player.sendMessage(lang.get("command.friend.accept.usage"));
                        return false;
                    }
                } else {
                    player.sendMessage(lang.get("command.friend.norequest"));
                    return false;
                }
            } else {
                player.sendMessage(lang.get("command.friend.usage"));
                return false;
            }
        } else {
            player.sendMessage(lang.get("command.friend.usage"));
            return false;
        }
        return false;
    }
}

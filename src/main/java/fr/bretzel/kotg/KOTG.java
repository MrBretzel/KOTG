package fr.bretzel.kotg;

import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.massivecore.ps.PS;

import fr.bretzel.kotg.command.CommandFriend;
import fr.bretzel.kotg.command.CommandKotg;
import fr.bretzel.kotg.command.CommandLanguage;
import fr.bretzel.kotg.command.CommandViewDistance;
import fr.bretzel.kotg.handlers.PlayerEvent;
import fr.bretzel.kotg.handlers.ZoneEvent;
import fr.bretzel.kotg.hologram.HologramManager;
import fr.bretzel.kotg.interfaces.ICommand;
import fr.bretzel.kotg.inventory.InventoryManager;
import fr.bretzel.kotg.language.Language;
import fr.bretzel.kotg.player.PlayerInfo;
import fr.bretzel.kotg.util.FilesUtils;
import fr.bretzel.kotg.zone.Selection;
import fr.bretzel.kotg.zone.Zone;
import fr.bretzel.kotg.zone.ZoneManager;

import org.bukkit.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Loïc Nussbaumer on 03/03/2017.
 */
public class KOTG extends JavaPlugin {

    public static HashMap<UUID, Selection> SELECTION_HASHMAP = new HashMap<>();

    public static HologramManager HOLOGRAM_MANAGER;

    public static File BASE_DIRECTORY;

    public static Plugin INSTANCE;

    public void onLoad() {
        INSTANCE = this;
        SELECTION_HASHMAP.clear();
        try {
            Language.enable();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onEnable() {

        BASE_DIRECTORY = getDataFolder();
        HOLOGRAM_MANAGER = new HologramManager(this);

        getServer().getPluginManager().registerEvents(new PlayerEvent(), this);
        getServer().getPluginManager().registerEvents(new ZoneEvent(), this);
        getServer().getPluginManager().registerEvents(new InventoryManager(), this);

        if (!BASE_DIRECTORY.exists()) {
            FilesUtils.createNewDirectory(BASE_DIRECTORY);
        }

        File zoneDirectory = new File(BASE_DIRECTORY, File.separator + "zones");

        if (!zoneDirectory.exists()) {
            FilesUtils.createNewDirectory(zoneDirectory);
        }

        ZoneManager.loadAll();

        registerCommand("kotg", new CommandKotg());
        registerCommand("friend", new CommandFriend());
        registerCommand("language", new CommandLanguage(), "lang");
        registerCommand("viewdistance", new CommandViewDistance());
    }

    public void onDisable() {
        SELECTION_HASHMAP.clear();

        File zoneDirectory = new File(BASE_DIRECTORY, File.separator + "zones");

        if (!zoneDirectory.exists()) {
            FilesUtils.createNewDirectory(zoneDirectory);
        }

        PlayerInfo.saveAll();
        ZoneManager.saveAll();
    }

    public static void addZoneToFaction(Faction faction, Zone zone) {
        for (Chunk c : zone.getChunks()) {
            zone.setFactionName(faction.getName());
            zone.setClaimed(true);
            BoardColl.get().setFactionAt(PS.valueOf(c), faction);
        }
    }

    public static void removeZoneToFaction(Zone zone) {
        for (Chunk c : zone.getChunks()) {
            zone.setClaimed(false);
            zone.setFactionName("null");
            BoardColl.get().setFactionAt(PS.valueOf(c), null);
        }
    }

    public static Selection getSelection(UUID player) {
        Selection selection = SELECTION_HASHMAP.get(player);
        if (selection == null) {
            selection = new Selection();
            SELECTION_HASHMAP.put(player, selection);
            return SELECTION_HASHMAP.get(player);
        }
        return selection;
    }

    private void registerCommand(String name, ICommand ICommand, String... alias) {
        getCommand(name.toLowerCase().trim()).setExecutor(ICommand);
        getCommand(name.toLowerCase().trim()).setTabCompleter(ICommand);

        if (alias.length > 0)
            getCommand(name).setAliases(Arrays.asList(alias));
    }
}

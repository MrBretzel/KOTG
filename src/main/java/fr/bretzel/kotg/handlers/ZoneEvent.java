package fr.bretzel.kotg.handlers;

import fr.bretzel.kotg.KOTG;
import fr.bretzel.kotg.event.zone.PlayerChangeZone;
import fr.bretzel.kotg.event.zone.PlayerJoinZone;
import fr.bretzel.kotg.event.zone.PlayerQuitZone;
import fr.bretzel.kotg.util.Title;
import fr.bretzel.kotg.zone.Zone;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

/**
 * Created by Loïc Nussbaumer on 10/03/2017.
 */
public class ZoneEvent implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoinZone(PlayerJoinZone event) {
        sendTitle(event.getPlayer(), event.getZone());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerChange(PlayerChangeZone event) {
        sendTitle(event.getPlayer(), event.getNewZone());
    }

    @EventHandler
    public void onPlayerQuitZone(PlayerQuitZone event) {

    }

    private void sendTitle(Player player, Zone zone) {
        Bukkit.getScheduler().runTaskLater(KOTG.INSTANCE, () -> Title.sendTitle(player, zone.getDisplayName(), zone.getFactionName(), 10 ,30, 10), 3);
    }
}

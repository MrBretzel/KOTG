package fr.bretzel.kotg.handlers;

import fr.bretzel.kotg.KOTG;
import fr.bretzel.kotg.event.zone.PlayerChangeZone;
import fr.bretzel.kotg.event.zone.PlayerJoinZone;
import fr.bretzel.kotg.event.zone.PlayerQuitZone;
import fr.bretzel.kotg.language.Language;
import fr.bretzel.kotg.player.PlayerInfo;
import fr.bretzel.kotg.zone.Zone;
import fr.bretzel.kotg.zone.ZoneManager;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.MainHand;

/**
 * Created by Loïc Nussbaumer on 10/03/2017.
 */
public class PlayerEvent implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        Location locTo = event.getTo();
        Location locFrom = event.getFrom();

        if (!ZoneManager.hasLocationInZone(locTo) && ZoneManager.hasLocationInZone(locFrom)) {
            //Quit a zone !
            Zone zone = ZoneManager.getZone(locFrom);
            PlayerQuitZone playerQuitZone = new PlayerQuitZone(player, zone);
            Bukkit.getPluginManager().callEvent(playerQuitZone);
        } else if (ZoneManager.hasLocationInZone(locTo) && ZoneManager.hasLocationInZone(locFrom)) {
            //ChangeZone !
            Zone oldZone = ZoneManager.getZone(locFrom);
            Zone newZone = ZoneManager.getZone(locTo);

            if (!oldZone.getName().equalsIgnoreCase(newZone.getName())) {
                PlayerChangeZone playerChangeZone = new PlayerChangeZone(player, ZoneManager.getZone(locTo), ZoneManager.getZone(locFrom));
                Bukkit.getPluginManager().callEvent(playerChangeZone);
            }
        } else if (ZoneManager.hasLocationInZone(locTo)) {
            //JoinZone !
            Zone zone = ZoneManager.getZone(locTo);
            PlayerJoinZone playerJoinZone = new PlayerJoinZone(player, zone);
            Bukkit.getPluginManager().callEvent(playerJoinZone);
        }
    }

    @EventHandler
    public void onPlayerRightClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getItem() != null && event.getItem().getType() == Material.NAME_TAG && player.getMainHand() == MainHand.RIGHT) {
            Language language = PlayerInfo.get(player).getLanguage();
            event.setCancelled(true);
            if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
                KOTG.getSelection(player.getUniqueId()).setLoc1(event.getClickedBlock().getLocation());
                player.sendMessage(language.get("selection.first"));
            } else if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                KOTG.getSelection(player.getUniqueId()).setLoc2(event.getClickedBlock().getLocation());
                player.sendMessage(language.get("selection.second"));
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        PlayerInfo.get(event.getPlayer());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        PlayerInfo info = PlayerInfo.get(event.getPlayer());
        if (info != null) {
            info.save();
            PlayerInfo.removePlayerInfo(event.getPlayer());
        }
    }
}

package fr.bretzel.kotg.util;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.chat.ComponentSerializer;

/**
 * Created by MrBretzel on 25/03/2017.
 */
public class ComponentUtil {

    public static BaseComponent[] toBaseComponent(String s) {
        return ComponentSerializer.parse(s);
    }
}

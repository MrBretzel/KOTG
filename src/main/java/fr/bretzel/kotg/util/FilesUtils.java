package fr.bretzel.kotg.util;

import com.google.common.collect.Lists;
import org.apache.commons.lang.Validate;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.NotDirectoryException;
import java.util.List;

/**
 * Created by Loïc Nussbaumer on 14/03/2017.
 */
public class FilesUtils {

    public static List<File> listFileDirectory(File file) {
        Validate.notNull(file);
        if (!file.isDirectory()) {
            try {
                throw new NotDirectoryException(file.toString());
            } catch (NotDirectoryException e) {
                e.printStackTrace();
            }
        }

        return Lists.newArrayList(file.listFiles());
    }

    public static void createNewFile(File file) {
        Validate.notNull(file);
        if (file.exists()) {
            try {
                throw new FileAlreadyExistsException(file.toString());
            } catch (FileAlreadyExistsException e) {
                e.printStackTrace();
            }
        }

        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createNewDirectory(File file) {
        Validate.notNull(file);
        if (file.exists()) {
            try {
                throw new FileAlreadyExistsException(file.toString());
            } catch (FileAlreadyExistsException e) {
                e.printStackTrace();
            }
        }

        file.mkdir();
    }
}

package fr.bretzel.kotg.util;

/**
 * Created by Loïc Nussbaumer on 14/03/2017.
 */
public class Utils {

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }

}

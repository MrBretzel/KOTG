package fr.bretzel.kotg.friend;

import fr.bretzel.kotg.event.friend.FriendRequestEvent;

import fr.bretzel.kotg.player.PlayerInfo;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.UUID;

/**
 * Created by Loïc Nussbaumer on 17/03/2017.
 */
public class FriendManager {

    private static HashSet<FriendRequest> friendRequest = new HashSet<>();

    public static void requestFriend(FriendRequest request) {
        if (hasAlreadySend(request.getSender()))
            friendRequest.remove(request.getSender());

        FriendRequestEvent event = new FriendRequestEvent(request);
        Bukkit.getPluginManager().callEvent(event);

        if (event.isCancelled())
            return;

        friendRequest.add(request);
    }

    public static boolean hasAlreadySend(UUID sender) {
        for (FriendRequest r : friendRequest) {
            if (r.getSender().toString().equalsIgnoreCase(sender.toString()))
                return true;
        }
        return false;
    }

    public static boolean hasReceiver(Player player) {
        for (FriendRequest request : friendRequest) {
            if (request.getReceiver().toString().equalsIgnoreCase(player.getUniqueId().toString()))
                return true;
        }
        return false;
    }

    public static boolean hasSender(Player player) {
        for (FriendRequest request : friendRequest) {
            if (request.getSender().toString().equalsIgnoreCase(player.getUniqueId().toString()))
                return true;
        }
        return false;
    }

    public static FriendRequest getRequestReceiver(Player player) {
        for (FriendRequest r : friendRequest) {
            if (r.getReceiver().toString().equalsIgnoreCase(player.getUniqueId().toString()))
                return r;
        }
        return null;
    }

    public static void remove(UUID sender, UUID toRemove) {
        remove(Bukkit.getPlayer(sender), Bukkit.getPlayer(toRemove));
    }

    public static void deleteFriendRequest(FriendRequest request) {
        friendRequest.remove(request);
    }

    public static void remove(Player sender, Player toRemove) {
        FriendRequestEvent.FriendRemoveEvent event = new FriendRequestEvent.FriendRemoveEvent(new FriendRequest(sender, toRemove));
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled())
            return;

        PlayerInfo s = PlayerInfo.get(sender);
        PlayerInfo t = PlayerInfo.get(toRemove);

        s.removeFriend(toRemove);
        t.removeFriend(sender);
    }

    public static void accept(Player receiver) {
        FriendRequest request = getRequestReceiver(receiver);
        request.accept();
        friendRequest.remove(request);
    }

    public static void denny(Player receiver) {
        FriendRequest request = getRequestReceiver(receiver);
        request.deny();
        friendRequest.remove(request);
    }
}

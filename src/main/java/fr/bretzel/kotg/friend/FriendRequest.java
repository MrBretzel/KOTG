package fr.bretzel.kotg.friend;

import fr.bretzel.kotg.event.friend.FriendRequestEvent;
import fr.bretzel.kotg.player.PlayerInfo;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.*;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Loïc Nussbaumer on 17/03/2017.
 */
public class FriendRequest {

    private UUID sender, receiver;
    private long timeStart;

    public FriendRequest(UUID sender, UUID receiver) {
        this.sender = sender;
        this.receiver = receiver;
        this.timeStart = System.nanoTime();
    }

    public FriendRequest(Player sender, Player requester) {
        this(sender.getUniqueId(), requester.getUniqueId());
    }

    public BaseComponent getSendMessage() {
        BaseComponent baseComponent = new TextComponent(Bukkit.getPlayer(sender).getDisplayName() + " asked to become a friend ! ");
        baseComponent.setColor(ChatColor.GREEN);
        TextComponent accept = new TextComponent("Accept");
        accept.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Accept friend request provided by " + Bukkit.getPlayer(sender).getDisplayName()).create()));
        accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend accept " + Bukkit.getPlayer(getSender()).getName()));
        accept.setBold(true);
        accept.setColor(ChatColor.DARK_GREEN);

        TextComponent deny = new TextComponent("Deny");
        deny.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Deny friend request provided by " + Bukkit.getPlayer(sender).getDisplayName()).create()));
        deny.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend deny " + Bukkit.getPlayer(getSender()).getName()));
        deny.setBold(true);
        deny.setColor(ChatColor.RED);

        baseComponent.addExtra(accept);

        TextComponent t = new TextComponent(new TextComponent(" | "));
        t.setColor(ChatColor.WHITE);

        baseComponent.addExtra(t);
        baseComponent.addExtra(deny);
        return baseComponent;
    }

    public void accept() {
        FriendRequestEvent.FriendAcceptRequest acceptRequest = new FriendRequestEvent.FriendAcceptRequest(this);

        if (acceptRequest.isCancelled())
            return;

        PlayerInfo sender = PlayerInfo.get(Bukkit.getPlayer(getSender()));
        PlayerInfo receiver = PlayerInfo.get(Bukkit.getPlayer(getReceiver()));

        sender.addFriend(receiver.getPlayer());
        receiver.addFriend(sender.getPlayer());
    }

    public void deny() {
        FriendRequestEvent.FriendDenyRequest denyRequest = new FriendRequestEvent.FriendDenyRequest(this);

        if (denyRequest.isCancelled())
            return;

        PlayerInfo sender = PlayerInfo.get(Bukkit.getPlayer(getSender()));
        PlayerInfo receiver = PlayerInfo.get(Bukkit.getPlayer(getReceiver()));

        sender.addFriend(receiver.getPlayer());
        receiver.addFriend(sender.getPlayer());
    }

    public UUID getReceiver() {
        return receiver;
    }

    public UUID getSender() {
        return sender;
    }

    public long getTimeStart() {
        return timeStart;
    }

    public boolean isExpired() {
        return getSecond() > 0x258;
    }

    public int getSecond() {
        long end = System.nanoTime();
        return (int) ((end - getTimeStart()) / 1000000000.0);
    }
}

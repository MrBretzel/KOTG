package fr.bretzel.kotg.event.friend;

import fr.bretzel.kotg.friend.FriendRequest;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by Loïc Nussbaumer on 17/03/2017.
 */
public class FriendRequestEvent extends Event implements Cancellable {

    private FriendRequest request;
    private boolean cancel = false;

    public FriendRequestEvent(FriendRequest request) {
        this.request = request;
    }

    public FriendRequest getRequest() {
        return request;
    }

    @Override
    public boolean isCancelled() {
        return cancel;
    }

    @Override
    public void setCancelled(boolean b) {
        this.cancel = b;
    }

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public static class FriendAcceptRequest extends FriendRequestEvent {

        public FriendAcceptRequest(FriendRequest request) {
            super(request);
        }

        private static final HandlerList handlers = new HandlerList();

        public HandlerList getHandlers() {
            return handlers;
        }

        public static HandlerList getHandlerList() {
            return handlers;
        }
    }

    public static class FriendDenyRequest extends FriendRequestEvent {

        public FriendDenyRequest(FriendRequest request) {
            super(request);
        }

        private static final HandlerList handlers = new HandlerList();

        public HandlerList getHandlers() {
            return handlers;
        }

        public static HandlerList getHandlerList() {
            return handlers;
        }
    }

    public static class FriendRemoveEvent extends FriendRequestEvent {

        public FriendRemoveEvent(FriendRequest request) {
            super(request);
        }

        private static final HandlerList handlers = new HandlerList();

        public HandlerList getHandlers() {
            return handlers;
        }

        public static HandlerList getHandlerList() {
            return handlers;
        }
    }
}

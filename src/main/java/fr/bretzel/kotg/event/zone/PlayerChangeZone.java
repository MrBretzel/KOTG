package fr.bretzel.kotg.event.zone;

import fr.bretzel.kotg.zone.Zone;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

/**
 * Created by Loïc Nussbaumer on 10/03/2017.
 */
public class PlayerChangeZone extends PlayerEvent {

    private Zone newZone, oldZone;

    public PlayerChangeZone(Player player, Zone newZone, Zone oldZone) {
        super(player);
        this.newZone = newZone;
        this.oldZone = oldZone;
    }

    public Zone getNewZone() {
        return newZone;
    }

    public Zone getOldZone() {
        return oldZone;
    }

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}

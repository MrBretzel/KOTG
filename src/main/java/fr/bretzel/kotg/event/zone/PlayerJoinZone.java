package fr.bretzel.kotg.event.zone;

import fr.bretzel.kotg.zone.Zone;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

/**
 * Created by Loïc Nussbaumer on 10/03/2017.
 */
public class PlayerJoinZone extends PlayerEvent {

    private Zone zone;

    public PlayerJoinZone(Player player, Zone zone) {
        super(player);
        this.zone = zone;
    }

    public Zone getZone() {
        return zone;
    }

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}

package fr.bretzel.kotg.zone;

import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.massivecore.ps.PS;
import fr.bretzel.kotg.KOTG;
import fr.bretzel.kotg.util.FilesUtils;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by Loïc Nussbaumer on 03/03/2017.
 */
public class ZoneManager {

    private static HashMap<String, Zone> zones = new HashMap<>();

    public static Zone getZone(String name) {
        return zones.get(name);
    }

    public static boolean hasZone(String name) {
        return getZone(name) != null;
    }

    public static void addZone(String name, Selection selection) {
        if (hasZone(name.trim()))
            return;
        zones.put(name.trim(), new Zone(selection, name.trim()));
    }

    public static void removeZone(String name) {
        Zone zone = zones.get(name);
        if (zone.isClaimed()) {
            for (Chunk chunk : zones.get(name).getChunks()) {
                PS ps = PS.valueOf(chunk);
                BoardColl.get().setFactionAt(ps, null);
            }
        }
        zones.remove(name);
    }

    public static void saveAll() {
        for (Zone zone : zones.values()) {
            File file = new File(new File(KOTG.BASE_DIRECTORY, File.separator + "zones"), zone.getName() + ".yml");

            if (!file.exists()) {
                FilesUtils.createNewFile(file);
            }

            YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);
            configuration.set("Name", zone.getName());
            configuration.set("Faction Name", zone.getFactionName());
            configuration.set("Display Name", zone.getDisplayName());
            configuration.set("Claimed", zone.isClaimed());
            configuration.set("Chunks", null);

            ConfigurationSection section = configuration.createSection("Chunks");

            int i = 0;

            for (Chunk c : zone.getChunks()) {
                section.set("Chunk " + i, toStringChunk(c));
                i++;
            }

            configuration.set("Chunks", section);

            try {
                configuration.save(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        zones.clear();
    }

    public static void loadAll() {
        zones.clear();
        for (File file : FilesUtils.listFileDirectory(new File(KOTG.BASE_DIRECTORY, File.separator + "zones"))) {
            if (!file.isDirectory()) {
                String name = file.getName();
                if (name.endsWith(".yml")) {
                    YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);

                    Zone zone = new Zone(configuration.getString("Name"));
                    zone.setFactionName(configuration.getString("Faction Name"));
                    zone.setClaimed(configuration.getBoolean("Claimed"));
                    zone.setDisplayName(configuration.getString("Display Name"));
                    ConfigurationSection section = configuration.getConfigurationSection("Chunks");

                    for (int i = 0; section.get("Chunk " + i) != null; i++) {

                        String s = section.getString("Chunk " + i);

                        Chunk chunk = toChunkString(s);
                        zone.addChunk(chunk);
                    }

                    if (zone.getDisplayName().equalsIgnoreCase("null"))
                        zone.setDisplayName(zone.getName());

                    zones.put(zone.getName(), zone);
                }
            }
        }
    }

    public static int listZize() {
        return zones.size();
    }

    public static Collection<Zone> getZones() {
        return zones.values();
    }

    public static String toStringChunk(Chunk chunk) {
        return (chunk.getWorld().getName() + ";" +
                chunk.getX() + ";") +
                chunk.getZ();
    }

    public static Zone getZone(Location location) {
        for (Zone zone : zones.values()) {
            if (zone.hasChunk(location))
                return zone;
        }
        return null;
    }

    public static boolean hasLocationInZone(Location location) {
        for (Zone zone : zones.values()) {
            if (zone.hasChunk(location))
                return true;
        }
        return false;
    }

    public static Chunk toChunkString(String string) {
        String[] strings = string.split(";");
        return Bukkit.getWorld(strings[0]).getChunkAt(Integer.valueOf(strings[1]), Integer.valueOf(strings[2]));
    }
}

package fr.bretzel.kotg.zone;

import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.massivecore.ps.PS;

import fr.bretzel.kotg.KOTG;
import fr.bretzel.kotg.hologram.Hologram;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Loïc Nussbaumer on 03/03/2017.
 */
public class Zone {

    private String name;
    private String factionName = "null";
    private String displayName;
    private boolean display = false;
    private boolean isClaimed = false;
    private ArrayList<Chunk> chunks = new ArrayList<>();

    private ArrayList<Location> holograms = new ArrayList<>();

    public Zone(String name) {
        this.name = name;

        setDisplayName(name);
    }

    public Zone(Selection selection, String name) {
        this.name = name;

        setDisplayName(name);

        chunks.clear();
        int maxX = (selection.getLoc1().getBlockX() < selection.getLoc2().getBlockX() ? selection.getLoc2().getBlockX() : selection.getLoc1().getBlockX());
        int minX = (selection.getLoc1().getBlockX() > selection.getLoc2().getBlockX() ? selection.getLoc2().getBlockX() : selection.getLoc1().getBlockX());

        int maxZ = (selection.getLoc1().getBlockZ() < selection.getLoc2().getBlockZ() ? selection.getLoc2().getBlockZ() : selection.getLoc1().getBlockZ());
        int minZ = (selection.getLoc1().getBlockZ() > selection.getLoc2().getBlockZ() ? selection.getLoc2().getBlockZ() : selection.getLoc1().getBlockZ());

        for (int x = minX; x <= maxX; x++) {
            for (int z = minZ; z <= maxZ; z++) {
                Location location = new Location(selection.getLoc1().getWorld(), x, selection.getLoc1().getY(), z);
                addChunk(location.getChunk());
            }
        }
    }

    public boolean isClaimed() {
        return isClaimed;
    }

    public void setClaimed(boolean claimed) {
        isClaimed = claimed;
    }

    public boolean isDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        if (display) {
            for (Chunk chunk : getChunks()) {
                int x = (chunk.getX() * 16) + 8;
                int z = (chunk.getZ() * 16) + 8;
                int y = chunk.getWorld().getHighestBlockYAt(x, z) + 3;
                Location location = new Location(chunk.getWorld(), x, y, z);
                String[] strings;

                if (isClaimed()) {
                    strings = new String[]{"   Zone name:   ",
                            ChatColor.GOLD + getName() + "   |   " + getDisplayName(),
                            "Is Claimed: " + ChatColor.AQUA + isClaimed(), "by faction :",
                            getFactionName()};
                } else {
                    strings = new String[]{"   Zone name:   ",
                            ChatColor.GOLD + getName() + "   |   " + getDisplayName(),
                            "Is Claimed: " + ChatColor.AQUA + isClaimed()};
                }

                Hologram hologram = new Hologram(location, strings, 0x1.ffbe279875574p-3, KOTG.HOLOGRAM_MANAGER);
                hologram.display(true);
                holograms.add(location);
            }
        } else {
            for (Location holo : holograms) {
                KOTG.HOLOGRAM_MANAGER.removeHologram(holo, 8);
            }
            holograms.clear();
        }
        this.display = display;
    }

    public List<Chunk> getChunks() {
        return chunks;
    }

    public boolean hasChunk(Location location) {
        return hasChunk(location.getChunk().getX(), location.getChunk().getZ());
    }

    public boolean hasChunk(int x, int z) {
        for (Chunk c : chunks) {
            if (c.getX() == x && c.getZ() == z)
                return true;
        }
        return false;
    }

    public void addChunk(Location chunk) {
        addChunk(chunk.getChunk());
    }

    public void addChunk(Chunk chunk) {
        if (hasChunk(chunk.getX(), chunk.getZ()))
            return;

        if (isClaimed()) {
            BoardColl.get().setFactionAt(PS.valueOf(chunk), FactionColl.get().getByName(factionName));
        }

        if (isDisplay()) {
            setDisplay(false);
            chunks.add(chunk);
            setDisplay(true);
        } else {
            chunks.add(chunk);
        }
    }

    public String getFactionName() {
        return factionName;
    }

    public void setFactionName(String factionName) {
        this.factionName = factionName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void removeChunk(Location chunk) {
        removeChunk(chunk.getChunk());
    }

    public void removeChunk(Chunk chunk) {
        if (isDisplay()) {
            setDisplay(false);
            chunks.remove(chunk);
            setDisplay(true);
        } else {
            chunks.remove(chunk);
        }
    }

    public String getName() {
        return name;
    }
}
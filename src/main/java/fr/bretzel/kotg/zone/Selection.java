package fr.bretzel.kotg.zone;

import fr.bretzel.kotg.particle.ParticleEffect;
import org.bukkit.Location;

/**
 * Created by Loïc Nussbaumer on 03/03/2017.
 */
public class Selection {

    private Location loc1, loc2;

    public Selection() {
    }

    public Selection(Location loc1, Location loc2) {
        setLoc2(loc2);
        setLoc1(loc1);
    }

    public Location getLoc2() {
        return loc2;
    }

    public Location getLoc1() {
        return loc1;
    }

    public void setLoc2(Location loc2) {
        this.loc2 = loc2;
        onSelected(loc2, true);
    }

    public void setLoc1(Location loc1) {
        this.loc1 = loc1;
        onSelected(loc1, false);
    }

    private void onSelected(Location newLoc, boolean isRight) {
        ParticleEffect effect = ParticleEffect.REDSTONE;
        ParticleEffect.OrdinaryColor color = isRight ? new ParticleEffect.OrdinaryColor(0, 0, 255) : new ParticleEffect.OrdinaryColor(0, 255, 0);
        for (Ordinal ordinal : Ordinal.values()) {
            if (ordinal == Ordinal.DOWN || ordinal == Ordinal.UP) {
                Location upAndDownC = ordinal.add(newLoc.clone());

                Location xLoc = upAndDownC.clone();
                Location zLoc = upAndDownC.clone();

                //new ParticleEffect.ParticlePacket(effect, color, true).sendTo(xLoc, 256);

                for (int i = 0; i < 10; i++) {
                    new ParticleEffect.ParticlePacket(effect, color, true).sendTo(xLoc, 256);
                    xLoc = xLoc.add(0.1, 0, 0);

                    new ParticleEffect.ParticlePacket(effect, color, true).sendTo(zLoc, 256);
                    zLoc = zLoc.add(0, 0, 0.1);

                }

                for (int i = 0; i < 10; i++) {
                    new ParticleEffect.ParticlePacket(effect, color, true).sendTo(xLoc, 256);
                    xLoc = xLoc.add(0, 0, 0.1);

                    new ParticleEffect.ParticlePacket(effect, color, true).sendTo(zLoc, 256);
                    zLoc = zLoc.add(0.1, 0, 0);

                }
            } else {
                Location center = ordinal.add(newLoc.clone());
                for (int i = 0; i < 10; i++) {
                    new ParticleEffect.ParticlePacket(effect, color, true).sendTo(center, 256);
                    center = center.add(0, 0.1, 0);
                }
            }
        }
    }

    private enum Ordinal {

        NORTH(0, 0, 0),
        SOUTH(1, 0, 1),
        WEST(0, 0, 1),
        EAST(1, 0, 0),
        UP(0, 1, 0),
        DOWN(0, 0, 0);

        private double x, y, z;

        Ordinal(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public double getX() {
            return x;
        }

        public double getY() {
            return y;
        }

        public double getZ() {
            return z;
        }

        public Location add(Location location) {
            return location.clone().add(getX(), getY(), getZ());
        }
    }
}
